﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppUser;

namespace UserAppUserTests.Resources
{
    public class MockUser
    {
        public static User GetGoodUser()
        {
            User user = new User();
            user.Active = true;
            user.DepartmentId = 1;
            user.Email = "choward@asu.edu";
            user.Name = "Carson";
            user.Phone = "6025155555";
            user.UserId = 1;
            user.UserName = "choward";
            return user;
        }
    }
}
