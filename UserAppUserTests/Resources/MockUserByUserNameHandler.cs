﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using UserAppUser.Queries;
using UserAppCore.Data;
using UserAppUser;

namespace UserAppUserTests.Resources
{
    public class MockUserByUserNameHandler : IQueryHandler<UserByUserNameQuery, User>
    {

        public static IQueryHandler<UserByUserNameQuery, User> ReturnUser(UserByUserNameQuery query)
        {
            var sub = Substitute.For<IQueryHandler<UserByUserNameQuery, User>>();
            sub.Handle(query).Returns(new User() { UserName = query.UserName });
            return sub;
        }



        public static IQueryHandler<UserByUserNameQuery, User> ReturnNullUser(UserByUserNameQuery query)
        {
            var sub = Substitute.For<IQueryHandler<UserByUserNameQuery, User>>();
            sub.Handle(query).Returns((User)null);
            return sub;
        }

        public User Handle(UserByUserNameQuery query)
        {
            return new User() { UserName = query.UserName, UserId = 999999 };
        }
    }
}
