﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppUser;
using UserAppCore.Data;
using System.Linq;

namespace UserAppUserTests.Resources
{
    public class TestDbConfig : DatabaseConfigurationOptions
    {
        public override string ConnectionString
        {
            get
            {
                return "Server=CHOWARD-PC;Database=UserApp;Trusted_Connection=True;";
            }

            set
            {
                value = value;
            }
        }
    }
}
