﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppUser.Commands;
using NSubstitute;
using UserAppCore.Data;

namespace UserAppUserTests
{
    [TestClass]
    public class SaveUserCommandHandlerTests
    {
        private SaveUserCommandHandler StubHandler()
        {
            var stubrepo = Substitute.For<IRepository<UserAppUser.User>>();
            var stubDb = Substitute.For<IDatabase>();
            var handler = new SaveUserCommandHandler(stubrepo, stubDb);
            return handler;
        }

        [TestMethod]
        public void Test_InsertUser_FromSave()
        {
            var handler = StubHandler();

            SaveUserCommand cmd = new SaveUserCommand()
            {
                UserToSave = new UserAppUser.User()
            };
            handler.Handle(cmd);

            Assert.IsTrue(true);
        }

        [TestMethod]
        public void Test_UpdateUser_FromSave()
        {
            var handler = StubHandler();

            SaveUserCommand cmd = new SaveUserCommand()
            {
                UserToSave = new UserAppUser.User() { UserId = 1000 }
            };
            handler.Handle(cmd);

            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_SaveUser_ArgumentNull()
        {
            var handler = StubHandler();

            handler.Handle(null);

            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(UserAppCore.Exceptions.InvalidCommandParameterException))]
        public void Test_SaveUser_ParameterNull()
        {
            var handler = StubHandler();

            handler.Handle(new SaveUserCommand());

            Assert.IsTrue(true);
        }
    }
}
