﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppUser;
using UserAppUserTests.Resources;
using NSubstitute;
using System.Linq;

namespace UserAppUserTests
{
    [TestClass]
    public class UserValidatorTests
    {
        [TestMethod]
        public void Test_Validate()
        {
            UserAppUser.Queries.UserByUserNameQuery query = new UserAppUser.Queries.UserByUserNameQuery()
            {
                UserName = "Joe"
            };
            User user = MockUser.GetGoodUser();

            UserValidator validator = new UserValidator(MockUserByUserNameHandler.ReturnUser(query));
            validator.Validate(user);
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void Test_NoName()
        {
            UserAppUser.Queries.UserByUserNameQuery query = new UserAppUser.Queries.UserByUserNameQuery()
            {
                UserName = "Joe"
            };
            User user = new User();
            user.UserName = "Joe";
            user.DepartmentId = 1;

            UserValidator validator = new UserValidator(MockUserByUserNameHandler.ReturnUser(query));
            var results = validator.Validate(user).FirstOrDefault(v => v.Key.Equals(nameof(user.Name)));
            Assert.IsNotNull(results);
        }

        [TestMethod]
        public void Test_UserName()
        {
            UserAppUser.Queries.UserByUserNameQuery query = new UserAppUser.Queries.UserByUserNameQuery()
            {
                UserName = "Joe"
            };
            User user = new User();
            user.Name = "Joe";
            user.DepartmentId = 1;

            UserValidator validator = new UserValidator(MockUserByUserNameHandler.ReturnUser(query));
            var results = validator.Validate(user).FirstOrDefault(v => v.Key.Equals(nameof(user.UserName)));
            Assert.IsNotNull(results);
        }

        [TestMethod]
        public void Test_UserName_Exists()
        {
            User user = new User();
            user.Name = "Joe";
            user.UserName = "Marc";
            user.UserId = 1;
            user.DepartmentId = 1;

            UserValidator validator = new UserValidator(new MockUserByUserNameHandler());
            var results = validator.Validate(user).FirstOrDefault(v => v.Key.Equals(nameof(user.UserName)));
            Assert.IsNotNull(results);
        }

        [TestMethod]
        public void Test_Department()
        {
            UserAppUser.Queries.UserByUserNameQuery query = new UserAppUser.Queries.UserByUserNameQuery()
            {
                UserName = "Joe"
            };
            User user = new User();
            user.Name = "Joe";
            user.UserName = "Marc";
            user.DepartmentId = -1;

            UserValidator validator = new UserValidator(MockUserByUserNameHandler.ReturnUser(query));
            var results = validator.Validate(user).FirstOrDefault(v => v.Key.Equals(nameof(user.DepartmentId)));
            Assert.IsNotNull(results);
        }
    }
}
