﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using UserAppCore.Data;
using UserAppUser;
using UserAppUser.Queries;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace UserAppUserTests
{
    [TestClass]
    public class GetAllUsersQueryHandlerTests
    {
        [TestMethod]
        public void Test_RunGetAllUsers()
        {
            var stubCn = Substitute.For<IDbConnection>();
            var stubDb = Substitute.For<IDatabase>();
            stubDb.CreateConnection().Returns(stubCn);
            var stubRepo = Substitute.For<IRepository<User>>();
            stubRepo.GetAll(stubCn).Returns(new List<User>() { new User() { UserId = 1 } });

            var handler = new GetAllUsersQueryHandler(stubDb, stubRepo);
            var results = handler.Handle(new GetAllUsersQuery());

            Assert.AreEqual(1, results.First().UserId); 
        }
    }
}
