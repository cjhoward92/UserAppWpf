﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppUser.Commands;
using NSubstitute;
using UserAppCore.Data;

namespace UserAppUserTests
{
    [TestClass]
    public class DeleteUserCommandHandlerTestss
    {
        private DeleteUserCommandHandler StubHandler()
        {
            var repoSub = Substitute.For<IRepository<UserAppUser.User>>();
            var handler = new DeleteUserCommandHandler(repoSub, Substitute.For<IDatabase>());
            return handler;
        }

        [TestMethod]
        public void Test_Delete()
        {
            var handler = StubHandler();

            DeleteUserCommand cmd = new DeleteUserCommand()
            {
                UserToDelete = new UserAppUser.User()
            };
            handler.Handle(cmd);
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_DeleteArgumentNull()
        {
            var handler = StubHandler();

            handler.Handle(null);
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(UserAppCore.Exceptions.InvalidCommandParameterException))]
        public void Test_DeleteParameterNull()
        {
            var handler = StubHandler();

            handler.Handle(new DeleteUserCommand());
            Assert.IsTrue(true);
        }
    }
}
