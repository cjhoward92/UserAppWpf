﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppUser;
using UserAppCore.Data;
using System.Linq;
using UserAppUserTests.Resources;

namespace UserAppUserTests
{
    [TestClass]
    public class UserRepositoryTests
    {
        private TestDbConfig testConfig = new TestDbConfig();

        [TestMethod]
        public void Test_LoadById()
        {
            SqlDatabase db = new SqlDatabase(testConfig);
            UserDapperRepository repo = new UserDapperRepository();

            var user = repo.GetById(db.CreateConnection(), 1);

            Assert.IsNotNull(user);
            Assert.AreEqual(1, user.UserId);
        }

        [TestMethod]
        public void Test_LoadById_BadId()
        {
            SqlDatabase db = new SqlDatabase(testConfig);
            UserDapperRepository repo = new UserDapperRepository();

            var user = repo.GetById(db.CreateConnection(), Int32.MaxValue);

            Assert.IsNull(user);
        }

        [TestMethod]
        public void Test_LoadAll()
        {
            SqlDatabase db = new SqlDatabase(testConfig);
            UserDapperRepository repo = new UserDapperRepository();

            var users = repo.GetAll(db.CreateConnection());

            Assert.IsNotNull(users);
            Assert.AreEqual(1, users.First().UserId);
        }

        [TestMethod]
        public void Test_Insert()
        {
            SqlDatabase db = new SqlDatabase(testConfig);
            UserDapperRepository repo = new UserDapperRepository();

            User user = new User();
            user.Name = "UNIQUE TEST";
            user.UserName = "UNIQUE";
            user.DepartmentId = 1;
            repo.Insert(db.CreateConnection(), user);

            repo.Delete(db.CreateConnection(), user);
            Assert.IsTrue(user.UserId > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.SqlClient.SqlException))]
        public void Test_DuplicateInsert()
        {
            SqlDatabase db = new SqlDatabase(testConfig);
            UserDapperRepository repo = new UserDapperRepository();

            User user = new User();
            user.Name = "TESTEST";
            user.UserName = "TESTESTEST";
            user.DepartmentId = 1;
            repo.Insert(db.CreateConnection(), user);
            repo.Insert(db.CreateConnection(), user);

            repo.Delete(db.CreateConnection(), user);
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void Test_Delete()
        {
            SqlDatabase db = new SqlDatabase(testConfig);
            UserDapperRepository repo = new UserDapperRepository();

            User user = new User();
            user.Name = "ZZZzzzZZZzzzZZZ";
            user.UserName = "ZZZzzzZZZzzzZZZ";
            user.DepartmentId = 1;
            repo.Insert(db.CreateConnection(), user);
            repo.Delete(db.CreateConnection(), user);

            Assert.IsNull(repo.GetById(db.CreateConnection(), user.UserId));
        }

        [TestMethod]
        public void Test_Update()
        {
            SqlDatabase db = new SqlDatabase(testConfig);
            UserDapperRepository repo = new UserDapperRepository();

            User user = new User();
            user.Name = "ZZZzzzZZZzzzZZZ";
            user.UserName = "ZZZzzzZZZzzzZZZ";
            user.DepartmentId = 1;
            user.Active = true;
            repo.Insert(db.CreateConnection(), user);

            user.Active = false;
            repo.Update(db.CreateConnection(), user);

            repo.Delete(db.CreateConnection(), user);
            Assert.IsFalse(user.Active);
        }
    }
}
