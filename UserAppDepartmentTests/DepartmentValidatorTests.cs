﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppCore.Data;
using UserAppDepartment;
using UserAppDepartment.Queries;
using NSubstitute;
using System.Linq;

namespace UserAppDepartmentTests
{
    [TestClass]
    public class DepartmentValidatorTests
    {
        [TestMethod]
        public void Test_DepartmentValidate_Valid()
        {
            DepartmentByDepartmentNameQuery query = new DepartmentByDepartmentNameQuery()
            {
                DepartmentName = "Dept1"
            };
            var handlerMock = Substitute.For<IQueryHandler<DepartmentByDepartmentNameQuery, Department>>();
            handlerMock.Handle(query).Returns((Department)null);

            var validator = new DepartmentValidator(handlerMock);
            Department dept = new Department();
            dept.DepartmentId = 1;
            dept.DepartmentName = "SUPER";

            var results = validator.Validate(dept);
            var first = results.FirstOrDefault();
            Assert.IsNull(first);
        }

        [TestMethod]
        public void Test_DepartmentValidate_NoName()
        {
            DepartmentByDepartmentNameQuery query = new DepartmentByDepartmentNameQuery()
            {
                DepartmentName = "Dept1"
            };
            var handlerMock = Substitute.For<IQueryHandler<DepartmentByDepartmentNameQuery, Department>>();
            handlerMock.Handle(query).Returns((Department)null);

            var validator = new DepartmentValidator(handlerMock);
            Department dept = new Department();
            dept.DepartmentId = 1;

            var results = validator.Validate(dept);
            var first = results.FirstOrDefault();
            Assert.IsTrue(first.Key.Equals(nameof(dept.DepartmentName)));
        }

        [TestMethod]
        public void Test_DepartmentValidate_NonUniqueName()
        {
            DepartmentByDepartmentNameQuery query = new DepartmentByDepartmentNameQuery()
            {
                DepartmentName = "Dept1",
            };
            var handlerMock = Substitute.For<IQueryHandler<DepartmentByDepartmentNameQuery, Department>>();
            handlerMock.Handle(query).Returns(new Department() { DepartmentName = "Dept1", DepartmentId = 1 });

            var validator = new DepartmentValidator(handlerMock);
            Department dept = new Department();
            dept.DepartmentId = 1;
            dept.DepartmentName = "Dept1";

            var results = validator.Validate(dept);
            var first = results.FirstOrDefault();
            Assert.IsNull(first);
        }

        public class MockHandler : IQueryHandler<DepartmentByDepartmentNameQuery, Department>
        {
            public Department Handle(DepartmentByDepartmentNameQuery query)
            {
                return new Department() { DepartmentId = 2 };
            }
        }

        [TestMethod]
        public void Test_DepartmentValidate_NonUniqueNameInvalid()
        {
            var validator = new DepartmentValidator(new MockHandler());
            Department dept = new Department();
            dept.DepartmentId = 1;
            dept.DepartmentName = "Dept1";

            var results = validator.Validate(dept);
            var first = results.FirstOrDefault();
            Assert.IsTrue(first.Key.Equals(nameof(dept.DepartmentName)));
        }
    }
}
