﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using UserAppCore.Data;
using UserAppDepartment;
using UserAppDepartment.Queries;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace UserAppDepartmentTests
{
    [TestClass]
    public class GetallDepartmentsQueryHandlerTests
    {
        [TestMethod]
        public void Test_RunGetAllDepartments()
        {
            var stubCn = Substitute.For<IDbConnection>();
            var stubDb = Substitute.For<IDatabase>();
            stubDb.CreateConnection().Returns(stubCn);
            var stubRepo = Substitute.For<IRepository<Department>>();
            stubRepo.GetAll(stubCn).Returns(new List<Department>() { new Department() { DepartmentId = 1 } });

            var handler = new GetAllDepartmentsQueryHandler(stubDb, stubRepo);
            var results = handler.Handle(new GetAllDepartmentsQuery());

            Assert.AreEqual(1, results.First().DepartmentId);
        }
    }
}
