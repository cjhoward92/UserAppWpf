﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppDepartment;
using UserAppDepartment.Commands;
using UserAppCore.Data;
using NSubstitute;

namespace UserAppDepartmentTests
{
    [TestClass]
    public class DeleteDepartmentCommandHandlerTests
    {
        private DeleteDepartmentCommandHandler StubHandler()
        {
            var repoSub = Substitute.For<IRepository<UserAppDepartment.Department>>();
            var handler = new DeleteDepartmentCommandHandler(repoSub, Substitute.For<IDatabase>());
            return handler;
        }

        [TestMethod]
        public void Test_Delete_Department()
        {
            var handler = StubHandler();

            DeleteDepartmentCommand cmd = new DeleteDepartmentCommand()
            {
                DepartmentToDelete = new Department()
            };
            handler.Handle(cmd);
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_DeleteArgumentNull_Department()
        {
            var handler = StubHandler();

            handler.Handle(null);
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(UserAppCore.Exceptions.InvalidCommandParameterException))]
        public void Test_DeleteParameterNull_Department()
        {
            var handler = StubHandler();

            handler.Handle(new DeleteDepartmentCommand());
            Assert.IsTrue(true);
        }
    }
}
