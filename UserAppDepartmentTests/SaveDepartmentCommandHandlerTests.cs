﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppDepartment;
using UserAppDepartment.Commands;
using NSubstitute;
using UserAppCore;
using UserAppCore.Data;

namespace UserAppDepartmentTests
{
    [TestClass]
    public class SaveDepartmentCommandHandlerTests
    {
        private SaveDepartmentCommandHandler StubHandler()
        {
            var stubrepo = Substitute.For<IRepository<Department>>();
            var stubDb = Substitute.For<IDatabase>();
            var handler = new SaveDepartmentCommandHandler(stubrepo, stubDb);
            return handler;
        }

        [TestMethod]
        public void Test_InsertUser_FromSave_Department()
        {
            var handler = StubHandler();

            SaveDepartmentCommand cmd = new SaveDepartmentCommand()
            {
                DepartmentToSave = new Department()
            };
            handler.Handle(cmd);

            Assert.IsTrue(true);
        }

        [TestMethod]
        public void Test_UpdateUser_FromSave_Department()
        {
            var handler = StubHandler();

            SaveDepartmentCommand cmd = new SaveDepartmentCommand()
            {
                DepartmentToSave = new Department() { DepartmentId = 1000 }
            };
            handler.Handle(cmd);

            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_SaveUser_ArgumentNull_Department()
        {
            var handler = StubHandler();

            handler.Handle(null);
        }

        [TestMethod]
        [ExpectedException(typeof(UserAppCore.Exceptions.InvalidCommandParameterException))]
        public void Test_SaveUser_ParameterNull_Department()
        {
            var handler = StubHandler();

            handler.Handle(new SaveDepartmentCommand());
        }
    }
}
