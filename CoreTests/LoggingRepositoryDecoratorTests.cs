﻿using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppCore.Data;
using UserAppCore.Decorators;
using NSubstitute;
using CoreTests.Resources;
using System.Collections.Generic;
using System.Linq;

namespace CoreTests
{
    [TestClass]
    public class LoggingRepositoryDecoratorTests
    {
        [TestMethod]
        public void Test_GetById()
        {
            var dbSub = Substitute.For<IDbConnection>();
            var sub = Substitute.For<IRepository<TestUser>>();
            sub.GetById(dbSub, 1).Returns(new TestUser() { Id = 1 });

            LoggingRepositoryDecorator<TestUser> dec = new LoggingRepositoryDecorator<TestUser>(sub, Substitute.For<UserAppCore.Logging.ILogger>());
            var result = dec.GetById(dbSub, 1);

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [TestMethod]
        public void Test_GetAll()
        {
            var dbSub = Substitute.For<IDbConnection>();
            var sub = Substitute.For<IRepository<TestUser>>();
            sub.GetAll(dbSub).Returns(new List<TestUser>() { new TestUser() { Id = 1 } });

            LoggingRepositoryDecorator<TestUser> dec = new LoggingRepositoryDecorator<TestUser>(sub, Substitute.For<UserAppCore.Logging.ILogger>());
            var result = dec.GetAll(dbSub);

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.First().Id);
        }

        [TestMethod]
        public void Test_Delete()
        {
            var dbSub = Substitute.For<IDbConnection>();
            var sub = Substitute.For<IRepository<TestUser>>();

            LoggingRepositoryDecorator<TestUser> dec = new LoggingRepositoryDecorator<TestUser>(sub, Substitute.For<UserAppCore.Logging.ILogger>());
            dec.Delete(dbSub, new TestUser());
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void Test_Update()
        {
            var dbSub = Substitute.For<IDbConnection>();
            var sub = Substitute.For<IRepository<TestUser>>();

            LoggingRepositoryDecorator<TestUser> dec = new LoggingRepositoryDecorator<TestUser>(sub, Substitute.For<UserAppCore.Logging.ILogger>());
            dec.Update(dbSub, new TestUser());
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void Test_Insert()
        {
            var dbSub = Substitute.For<IDbConnection>();
            var sub = Substitute.For<IRepository<TestUser>>();

            LoggingRepositoryDecorator<TestUser> dec = new LoggingRepositoryDecorator<TestUser>(sub, Substitute.For<UserAppCore.Logging.ILogger>());
            dec.Insert(dbSub, new TestUser());
            Assert.IsTrue(true);
        }
    }
}
