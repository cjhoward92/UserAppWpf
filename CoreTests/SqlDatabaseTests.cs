﻿using System;
using UserAppCore.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreTests
{
    [TestClass]
    public class SqlDatabaseTests
    {
        public class TestDbOptions : DatabaseConfigurationOptions
        {
            public override string ConnectionString
            {
                get
                {
                    return "Server=CHOWARD-PC;Database=UserApp;Trusted_Connection=True;";
                }

                set
                {
                    string str = value;
                }
            }
        }

        [TestMethod]
        public void Test_CreateConnection()
        {
            SqlDatabase db = new SqlDatabase(new TestDbOptions());

            var cn = db.CreateConnection();

            Assert.IsNotNull(cn);
            cn.Dispose();
        }

        [TestMethod]
        public void Test_OpenConnection()
        {
            SqlDatabase db = new SqlDatabase(new TestDbOptions());

            var cn = db.CreateConnection();
            cn.Open();

            Assert.IsTrue(cn.State == System.Data.ConnectionState.Open);
            cn.Close();
            cn.Dispose();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_BadConnection()
        {
            SqlDatabase db = new SqlDatabase(new DatabaseConfigurationOptions());

            var cn = db.CreateConnection();
            cn.Open();

            Assert.IsTrue(cn.State == System.Data.ConnectionState.Open);
            cn.Close();
            cn.Dispose();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_BadConnectionNullOptions()
        {
            SqlDatabase db = new SqlDatabase(null);

            var cn = db.CreateConnection();
            cn.Open();

            Assert.IsTrue(cn.State == System.Data.ConnectionState.Open);
            cn.Close();
            cn.Dispose();
        }
    }
}
