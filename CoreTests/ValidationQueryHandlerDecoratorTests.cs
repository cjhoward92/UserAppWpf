﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppCore.Decorators;
using UserAppCore.Data;
using NSubstitute;
using System.ComponentModel.DataAnnotations;
using CoreTests.Resources;

namespace CoreTests
{
    [TestClass]
    public class ValidationQueryHandlerDecoratorTests
    {
        public class BadQuery : IQuery<TestUser>
        {
            [MaxLength(5)]
            public string Filter { get; set; }
        }

        private IQueryHandler<IQuery<TestUser>, TestUser> GetHandler(IQuery<TestUser> subQuery)
        {
            var handlerSub = Substitute.For<IQueryHandler<IQuery<TestUser>, TestUser>>();
            handlerSub.Handle(subQuery).Returns(new TestUser() { Id = 1 });
            return handlerSub;
        }

        [TestMethod]
        public void Test_Handle()
        {
            var subQuery = Substitute.For<IQuery<TestUser>>();

            ValidationQueryHandlerDecorator<IQuery<TestUser>, TestUser> dec =
                new ValidationQueryHandlerDecorator<IQuery<TestUser>, TestUser>(GetHandler(subQuery));

            var result = dec.Handle(subQuery);

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ComponentModel.DataAnnotations.ValidationException))]
        public void Test_HandleException()
        {
            var subQuery = new BadQuery() { Filter = "ZZZZZZZZZZ" };

            ValidationQueryHandlerDecorator<IQuery<TestUser>, TestUser> dec =
                new ValidationQueryHandlerDecorator<IQuery<TestUser>, TestUser>(GetHandler(subQuery));

            var result = dec.Handle(subQuery);

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }
    }
}
