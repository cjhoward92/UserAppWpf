﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppCore.Decorators;
using UserAppCore.Logging;
using NSubstitute;

namespace CoreTests
{
    [TestClass]
    public class DateTimeLoggerDecoratorTests
    {
        [TestMethod]
        public void Test_Log()
        {
            DateTimeLoggerDecorator logger = new DateTimeLoggerDecorator(Substitute.For<ILogger>());
            logger.Log("Message");
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_LogFail()
        {
            DateTimeLoggerDecorator logger = new DateTimeLoggerDecorator(Substitute.For<ILogger>());
            logger.Log(null);
            Assert.IsTrue(true);
        }
    }
}
