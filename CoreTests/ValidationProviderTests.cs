﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppCore.Data;
using NSubstitute;
using NSubstitute.Core;
using NSubstitute.ReturnsExtensions;
using System.Collections.Generic;

namespace CoreTests
{
    [TestClass]
    public class ValidationProviderTests
    {
        public static IValidator GetValidator(Type type)
        {
            return Substitute.For<IValidator>();
        }
        public static IValidator GetTestValidator(Type type)
        {
            return new TestValidator();
        }

        public class TestValidator : IValidator
        {
            public IEnumerable<ValidationResult> Validate(object entity)
            {
                yield return new ValidationResult("M", "MM");
            }
        }

        [TestMethod]
        public void Test_Validate()
        {
            ValidationProvider provider = new ValidationProvider(GetValidator);
            provider.Validate(new object());
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void Test_ValidateAll()
        {
            ValidationProvider provider = new ValidationProvider(ValidationProviderTests.GetValidator);
            provider.ValidateAll(new[] { new object(), new object() });
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void Test_FailedValidate()
        {
            ValidationProvider provider = new ValidationProvider(GetTestValidator);
            provider.Validate(new object());
            Assert.IsTrue(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void Test_FailedValidateAll()
        {
            ValidationProvider provider = new ValidationProvider(GetTestValidator);
            provider.ValidateAll(new[] { new object(), new object() });
            Assert.IsTrue(false);
        }
    }
}
