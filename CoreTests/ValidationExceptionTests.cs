﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppCore.Data;
using UserAppCore;
using System.Linq;
using System.Collections.Generic;

namespace CoreTests
{
    [TestClass]
    public class ValidationExceptionTests
    {
        private static List<ValidationResult> MakeList()
        {
            var list = new List<ValidationResult>()
            {
                new ValidationResult("Derp", "Message1"),
                new ValidationResult("Derp2", "Message2")
            };
            return list;
        }

        [TestMethod]
        public void Test_ExceptionConstructor()
        {
            var list = MakeList();
            ValidationException excp = new ValidationException(list);
            Assert.AreEqual("Message1", excp.Message);
        }
    }
}
