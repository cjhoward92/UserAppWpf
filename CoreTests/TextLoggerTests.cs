﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using UserAppCore.Logging;

namespace CoreTests
{
    [TestClass]
    public class TextLoggerTests
    {
        private TestTextLogContext context = new TestTextLogContext();

        public class TestTextLogContext : TextLogContext
        {
            private string path;

            public TestTextLogContext()
            {
                path = Path.GetTempPath() + "." + Path.GetRandomFileName() + ".txt";
            }

            public override string FilePath
            {
                get
                {
                    return path;
                }
            }
        }
        private void CleanFile()
        {
            try
            {
                File.Delete(context.FilePath);
            }
            catch
            {

            }
        }

        [TestMethod]
        public void Test_Log()
        {
            TextLogger logger = new TextLogger(context);

            logger.Log("Message");

            Assert.IsTrue(System.IO.File.Exists(context.FilePath));
            CleanFile();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_Log_Fail()
        {
            TextLogger logger = new TextLogger(context);

            logger.Log(null);

            Assert.IsTrue(true);
            CleanFile();
        }
    }
}
