﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

using UserAppCore.Data;

namespace CoreTests
{
    [TestClass]
    public class DefaultValidatorTests
    {
        [TestMethod]
        public void Test_ReturnEmpty()
        {
            DefaultValidator<int> validator = new DefaultValidator<int>();
            var enumeration = validator.Validate(1);
            Assert.AreEqual(Enumerable.Empty<ValidationResult>(), enumeration);
        }
    }
}
