﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;
using UserAppDepartment;

namespace UserAppDepartment.Commands
{
    public class DeleteDepartmentCommand : ICommand
    {
        public Department DepartmentToDelete { get; set; }
    }
}
