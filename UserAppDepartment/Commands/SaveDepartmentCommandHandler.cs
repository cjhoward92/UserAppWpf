﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;
using UserAppCore.Exceptions;

namespace UserAppDepartment.Commands
{
    public class SaveDepartmentCommandHandler : ICommandHandler<SaveDepartmentCommand>
    {
        private readonly IRepository<Department> departmentRepository;
        private readonly IDatabase database;

        public SaveDepartmentCommandHandler(IRepository<Department> departmentRepository, IDatabase database)
        {
            this.departmentRepository = departmentRepository;
            this.database = database;
        }

        public void Handle(SaveDepartmentCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));
            if (command.DepartmentToSave == null)
                throw new InvalidCommandParameterException(command, nameof(command.DepartmentToSave), null);

            using (var cn = this.database.CreateConnection())
            {
                if (command.DepartmentToSave.DepartmentId <= 0)
                    this.departmentRepository.Insert(cn, command.DepartmentToSave);
                else
                    this.departmentRepository.Update(cn, command.DepartmentToSave);
            }
        }
    }
}
