﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;
using UserAppCore.Exceptions;
using UserAppDepartment;

namespace UserAppDepartment.Commands
{
    public class DeleteDepartmentCommandHandler : ICommandHandler<DeleteDepartmentCommand>
    {
        private readonly IRepository<Department> departmentRepository;
        private readonly IDatabase database;

        public DeleteDepartmentCommandHandler(IRepository<Department> departmentRepository, IDatabase database)
        {
            this.departmentRepository = departmentRepository;
            this.database = database;
        }

        public void Handle(DeleteDepartmentCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));
            if (command.DepartmentToDelete == null)
                throw new InvalidCommandParameterException(command, nameof(command.DepartmentToDelete), null);

            using (var cn = this.database.CreateConnection())
            {
                this.departmentRepository.Delete(cn, command.DepartmentToDelete);
            }
        }
    }
}
