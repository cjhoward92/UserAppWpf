﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;
using UserAppCore.Exceptions;
using UserAppDepartment;

namespace UserAppDepartment.Commands
{
    public class SaveDepartmentCommand : ICommand
    {
        public Department DepartmentToSave { get; set; }
    }
}
