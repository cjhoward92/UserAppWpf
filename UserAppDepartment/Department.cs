﻿using System;
using System.ComponentModel.DataAnnotations;
using Dapper.Contrib.Extensions;

namespace UserAppDepartment
{
    public class Department
    {
        [Dapper.Contrib.Extensions.Key]
        public int DepartmentId { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string DepartmentName { get; set; }

        public string DepartmentPhone { get; set; }

        [MaxLength(100)]
        public string DepartmentAddress1 { get; set; }

        [MaxLength(100)]
        public string DepartmentAddress2 { get; set; }

        [MaxLength(50)]
        public string DepartmentCity { get; set; }

        [MaxLength(2)]
        public string DepartmentState { get; set; }

        [MaxLength(12)]
        public string DepartmentZip { get; set; }
    }
}
