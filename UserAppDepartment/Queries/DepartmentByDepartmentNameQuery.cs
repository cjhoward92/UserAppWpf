﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;

namespace UserAppDepartment.Queries
{
    public class DepartmentByDepartmentNameQuery : IQuery<Department>
    {
        public string DepartmentName { get; set; }
    }
}
