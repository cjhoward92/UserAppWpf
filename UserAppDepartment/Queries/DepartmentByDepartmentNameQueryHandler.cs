﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using UserAppCore.Data;

namespace UserAppDepartment.Queries
{
    public class DepartmentByDepartmentNameQueryHandler : IQueryHandler<DepartmentByDepartmentNameQuery, Department>
    {
        private readonly IDatabase database;

        public DepartmentByDepartmentNameQueryHandler(IDatabase database)
        {
            this.database = database;
        }

        public Department Handle(DepartmentByDepartmentNameQuery query)
        {
            string sql = "SELECT * FROM Departments WHERE DepartmentName = @DepartmentName";
            using (var cn = this.database.CreateConnection())
            {
                return cn.Query<Department>(sql, new { DepartmentName = query.DepartmentName }).FirstOrDefault();
            }
        }
    }
}
