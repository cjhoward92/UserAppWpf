﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;

namespace UserAppDepartment.Queries
{
    public class GetAllDepartmentsQueryHandler : IQueryHandler<GetAllDepartmentsQuery, IEnumerable<Department>>
    {
        private readonly IDatabase database;
        private readonly IRepository<Department> departmentRepository;

        public GetAllDepartmentsQueryHandler(IDatabase database, IRepository<Department> departmentRepository)
        {
            this.database = database;
            this.departmentRepository = departmentRepository;
        }

        public IEnumerable<Department> Handle(GetAllDepartmentsQuery query)
        {
            using (var cn = this.database.CreateConnection())
            {
                return this.departmentRepository.GetAll(cn);
            }
        }
    }
}
