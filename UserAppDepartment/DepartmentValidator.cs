﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserAppDepartment.Queries;
using UserAppCore.Data;

namespace UserAppDepartment
{
    public class DepartmentValidator : Validator<Department>
    {
        private readonly IQueryHandler<DepartmentByDepartmentNameQuery, Department> nameQueryHandler;

        public DepartmentValidator(IQueryHandler<DepartmentByDepartmentNameQuery, Department> nameQueryHandler)
        {
            this.nameQueryHandler = nameQueryHandler;
        }

        protected override IEnumerable<ValidationResult> Validate(Department entity)
        {
            if (String.IsNullOrWhiteSpace(entity.DepartmentName))
                yield return new ValidationResult(nameof(entity.DepartmentName), $"{nameof(entity.DepartmentName)} cannot be blank");

            DepartmentByDepartmentNameQuery nameQuery = new DepartmentByDepartmentNameQuery()
            {
                DepartmentName = entity.DepartmentName
            };
            var dept = nameQueryHandler.Handle(nameQuery);
            if (dept != null && dept.DepartmentId != entity.DepartmentId)
                yield return new ValidationResult(nameof(entity.DepartmentName), $"{nameof(entity.DepartmentName)} {entity.DepartmentName} already exists");
        }
    }
}
