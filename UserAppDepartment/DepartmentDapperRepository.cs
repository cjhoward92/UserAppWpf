﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using UserAppCore.Data;
using Dapper;
using Dapper.Mapper;
using Dapper.Contrib.Extensions;

namespace UserAppDepartment
{
    public class DepartmentDapperRepository : IRepository<Department>
    {
        public DepartmentDapperRepository() { }

        public void Delete(IDbConnection cn, Department entity)
        {
            cn.Delete(entity);
        }

        public IEnumerable<Department> GetAll(IDbConnection cn)
        {
            return cn.GetAll<Department>();
        }

        public Department GetById(IDbConnection cn, Int32 id)
        {
            return cn.Get<Department>(id);
        }

        public void Insert(IDbConnection cn, Department entity)
        {
            entity.DepartmentId = (int)cn.Insert<Department>(entity);
        }

        public void Update(IDbConnection cn, Department entity)
        {
            cn.Update<Department>(entity);
        }
    }
}
