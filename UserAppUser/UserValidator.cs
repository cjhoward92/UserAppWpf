﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppUser.Queries;
using UserAppCore.Data;

namespace UserAppUser
{
    public class UserValidator : Validator<User>
    {
        private readonly IQueryHandler<UserByUserNameQuery, User> userByUserNameHandler;

        public UserValidator(IQueryHandler<UserByUserNameQuery, User> userByUserNameHandler)
        {
            this.userByUserNameHandler = userByUserNameHandler;
        }

        protected override IEnumerable<ValidationResult> Validate(User entity)
        {
            if (String.IsNullOrWhiteSpace(entity.Name))
                yield return new ValidationResult(nameof(entity.Name), $"{nameof(entity.Name)} cannot be blank");

            if (String.IsNullOrWhiteSpace(entity.UserName))
                yield return new ValidationResult(nameof(entity.UserName), $"{nameof(entity.UserName)} cannot be blank");

            UserByUserNameQuery query = new UserByUserNameQuery()
            {
                UserName = entity.UserName
            };
            var usr = this.userByUserNameHandler.Handle(query);
            if (usr != null && usr.UserId != entity.UserId)
                yield return new ValidationResult(nameof(entity.UserName), $"{nameof(entity.UserName)} '{query.UserName}' already exists");

            if (entity.DepartmentId <= 0)
                yield return new ValidationResult(nameof(entity.DepartmentId), $"{nameof(entity.DepartmentId)} must be a valid department");
        }
    }
}
