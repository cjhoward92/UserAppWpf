﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;
using UserAppCore.Exceptions;

namespace UserAppUser.Commands
{
    public class SaveUserCommandHandler : ICommandHandler<SaveUserCommand>
    {
        private readonly IRepository<User> userRepository;
        private readonly IDatabase database;

        public SaveUserCommandHandler(IRepository<User> userRepository, IDatabase database)
        {
            this.userRepository = userRepository;
            this.database = database;
        }

        public void Handle(SaveUserCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));
            if (command.UserToSave == null)
                throw new InvalidCommandParameterException(command, nameof(command.UserToSave), null);

            using (var cn = this.database.CreateConnection())
            {
                if (command.UserToSave.UserId <= 0)
                    this.userRepository.Insert(cn, command.UserToSave);
                else
                    this.userRepository.Update(cn, command.UserToSave);
            }
        }
    }
}
