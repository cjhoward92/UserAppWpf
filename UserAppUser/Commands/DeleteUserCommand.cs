﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;

namespace UserAppUser.Commands
{
    public class DeleteUserCommand : ICommand
    {
        public User UserToDelete { get; set; }
    }
}
