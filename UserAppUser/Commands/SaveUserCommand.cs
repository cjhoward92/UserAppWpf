﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;
using UserAppCore.Exceptions;

namespace UserAppUser.Commands
{
    public class SaveUserCommand : ICommand
    {
        public User UserToSave { get; set; }
    }
}
