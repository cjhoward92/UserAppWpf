﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;
using UserAppCore.Exceptions;

namespace UserAppUser.Commands
{
    public class DeleteUserCommandHandler : ICommandHandler<DeleteUserCommand>
    {
        private readonly IRepository<User> userRepository;
        private readonly IDatabase database;

        public DeleteUserCommandHandler(IRepository<User> userRepository, IDatabase database)
        {
            this.userRepository = userRepository;
            this.database = database;
        }

        public void Handle(DeleteUserCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));
            if (command.UserToDelete == null)
                throw new InvalidCommandParameterException(command, nameof(command.UserToDelete), null);

            using (var cn = this.database.CreateConnection())
            {
                this.userRepository.Delete(cn, command.UserToDelete);
            }
        }
    }
}
