﻿using System;
using System.ComponentModel.DataAnnotations;
using UserAppDepartment;

namespace UserAppUser
{
    public class User
    {
        [Dapper.Contrib.Extensions.Key]
        public int UserId { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(20)]
        public string UserName { get; set; }

        [Required]
        [MinLength(20)]
        public string Name { get; set; }

        [MinLength(6)]
        public string Email { get; set; }

        [MinLength(10)]
        [MaxLength(25)]
        public string Phone { get; set; }

        [Required]
        public bool Active { get; set; }

        [Required]
        public int DepartmentId { get; set; }
        //public Department Department { get; set; }
    }
}
