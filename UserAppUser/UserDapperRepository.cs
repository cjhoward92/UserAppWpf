﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using Dapper.Contrib.Extensions;
using Dapper.Mapper;
using UserAppCore.Data;
using UserAppDepartment;

namespace UserAppUser
{
    public class UserDapperRepository : IRepository<User>
    {
        public UserDapperRepository() { }

        public void Delete(IDbConnection cn, User entity)
        {
            cn.Delete(entity);
        }

        public User GetById(IDbConnection cn, Int32 id)
        {
            return cn.Get<User>(id);
        }

        public IEnumerable<User> GetAll(IDbConnection cn)
        {
            return cn.GetAll<User>();
        }

        public void Insert(IDbConnection cn, User entity)
        {
            cn.Insert(entity);
        }

        public void Update(IDbConnection cn, User entity)
        {
            cn.Update(entity);
        }
    }
}
