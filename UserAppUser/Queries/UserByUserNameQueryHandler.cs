﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using UserAppCore.Data;

namespace UserAppUser.Queries
{
    public class UserByUserNameQueryHandler : IQueryHandler<UserByUserNameQuery, User>
    {
        private readonly IDatabase database;

        public UserByUserNameQueryHandler(IDatabase database)
        {
            this.database = database;
        }

        public User Handle(UserByUserNameQuery query)
        {
            string sql = "SELECT * FROM Users WHERE UserName = @UserName";
            using (var cn = this.database.CreateConnection())
            {
                return cn.Query<User>(sql, new { UserName = query.UserName }).FirstOrDefault();
            }
        }
    }
}
