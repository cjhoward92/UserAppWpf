﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;

namespace UserAppUser.Queries
{
    public class UserByUserNameQuery : IQuery<User>
    {
        public string UserName { get; set; }
    }
}
