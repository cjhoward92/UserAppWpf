﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;

namespace UserAppUser.Queries
{
    public class GetAllUsersQueryHandler : IQueryHandler<GetAllUsersQuery, IEnumerable<User>>
    {
        private readonly IDatabase database;
        private readonly IRepository<User> userRepository;

        public GetAllUsersQueryHandler(IDatabase database, IRepository<User> userRepository)
        {
            this.database = database;
            this.userRepository = userRepository;
        }

        public IEnumerable<User> Handle(GetAllUsersQuery query)
        {
            using (var cn = this.database.CreateConnection())
            {
                return this.userRepository.GetAll(cn);
            }
        }
    }
}
