﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace UserAppWpf.Converters
{
    public class BooleanToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));
            if (!(value is bool))
                throw new InvalidCastException($"Cannot cast {nameof(value)} to type {typeof(bool).FullName}");

            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool val = true;
            if (!Boolean.TryParse(value as String, out val))
                throw new InvalidCastException($"Cannot cast {nameof(value)} to type {typeof(string).FullName}");
            return val;
        }
    }
}
