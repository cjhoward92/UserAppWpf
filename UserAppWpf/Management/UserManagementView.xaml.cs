﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using UserAppUser;
using UserAppWpf.Framework;

namespace UserAppWpf.Management
{
    /// <summary>
    /// Interaction logic for UserManagementView.xaml
    /// </summary>
    public partial class UserManagementView : Window
    {
        public UserManagementView()
        {
            InitializeComponent();
            this.Loaded += UserManagementView_Loaded;
        }

        private void UserManagementView_Loaded(object sender, RoutedEventArgs e)
        {
            var viewModel = this.DataContext as UserManagementViewModel;
            viewModel.LoadUsers();
        }

        private void ListView_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var viewModel = this.DataContext as UserManagementViewModel;
            if (viewModel == null)
                throw new UserAppWpf.Framework.Exceptions.InvalidViewModelException(this.DataContext.GetType());

            viewModel.EditSelectedUser();
        }
    }
}
