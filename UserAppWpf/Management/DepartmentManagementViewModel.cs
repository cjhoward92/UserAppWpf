﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserAppWpf.Framework;
using UserAppWpf.Components;
using UserAppCore.Data;
using UserAppDepartment;
using UserAppDepartment.Queries;

namespace UserAppWpf.Management
{
    public class DepartmentManagementViewModel : ViewModelBase
    {
        private readonly DelegateCommand newCommand;
        private readonly IEditProcessor editProcessor;
        private readonly IQueryHandler<GetAllDepartmentsQuery, IEnumerable<Department>> getAllDepartmentsQueryHandler;

        private Department selectedDepartment;
        private List<Department> departments;

        public Department SelectedDepartment
        {
            get { return this.selectedDepartment; }
            set
            {
                this.selectedDepartment = value;
                this.NotifyPropertyChanged(nameof(SelectedDepartment));
            }
        }
        public List<Department> Departments
        {
            get
            {
                return this.departments;
            }
        }
        public DelegateCommand NewCommand
        {
            get { return this.newCommand; }
        }

        public DepartmentManagementViewModel(IEditProcessor editProcessor,
            IQueryHandler<GetAllDepartmentsQuery, IEnumerable<Department>> getAllDepartmentsQueryHandler)
        {
            this.newCommand = new DelegateCommand(this.ExecuteNew, (parmeter) => true);

            this.editProcessor = editProcessor;
            this.getAllDepartmentsQueryHandler = getAllDepartmentsQueryHandler;
        }

        public void LoadDepartments()
        {
            this.departments = new List<Department>(this.getAllDepartmentsQueryHandler.Handle(new GetAllDepartmentsQuery()));
            this.NotifyPropertyChanged(nameof(Departments));
        }

        public void EditSelectedDepartment()
        {
            if (this.selectedDepartment == null)
                throw new Exception("No user selected");

            this.editProcessor.EditItem<Department>(this.SelectedDepartment);
        }
        public void ExecuteNew(object parameter)
        {
            this.editProcessor.EditItem<Department>(new Department());
        }
    }
}
