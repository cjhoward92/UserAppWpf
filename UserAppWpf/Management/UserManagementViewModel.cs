﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserAppUser;
using UserAppUser.Queries;
using UserAppWpf.Framework;
using UserAppCore.Data;
using UserAppWpf.Components;

namespace UserAppWpf.Management
{
    public class UserManagementViewModel : ViewModelBase
    {
        private readonly DelegateCommand newCommand;
        private readonly IEditProcessor editProcessor;
        private readonly IQueryHandler<GetAllUsersQuery, IEnumerable<User>> getAllUsersQueryHandler;

        private User selectedUser;
        private List<User> users;

        public User SelectedUser
        {
            get { return this.selectedUser; }
            set
            {
                this.selectedUser = value;
                this.NotifyPropertyChanged(nameof(SelectedUser));
            }
        }
        public List<User> Users
        {
            get
            {
                return this.users;
            }
        }
        public DelegateCommand NewCommand
        {
            get { return this.newCommand; }
        }

        public UserManagementViewModel(IEditProcessor editProcessor,
            IQueryHandler<GetAllUsersQuery, IEnumerable<User>> getAllUsersQueryHandler)
        {
            this.newCommand = new DelegateCommand(this.ExecuteNew, (parmeter) => true);

            this.editProcessor = editProcessor;
            this.getAllUsersQueryHandler = getAllUsersQueryHandler;
        }

        public void LoadUsers()
        {
            this.users = new List<User>(this.getAllUsersQueryHandler.Handle(new GetAllUsersQuery()));
            this.NotifyPropertyChanged(nameof(Users));
        }

        public void EditSelectedUser()
        {
            if (this.selectedUser == null)
                throw new Exception("No user selected");

            this.editProcessor.EditItem<User>(this.SelectedUser);
        }
        public void ExecuteNew(object parameter)
        {
            this.editProcessor.EditItem<User>(new User());
        }
    }
}
