﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UserAppWpf.Management
{
    /// <summary>
    /// Interaction logic for DepartmentManagementView.xaml
    /// </summary>
    public partial class DepartmentManagementView : Window
    {
        public DepartmentManagementView()
        {
            InitializeComponent();
            this.Loaded += DepartmentManagementView_Loaded;
        }

        private void DepartmentManagementView_Loaded(object sender, RoutedEventArgs e)
        {
            var viewModel = this.DataContext as DepartmentManagementViewModel;
            viewModel.LoadDepartments();
        }

        private void ListView_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var viewModel = this.DataContext as DepartmentManagementViewModel;
            if (viewModel == null)
                throw new UserAppWpf.Framework.Exceptions.InvalidViewModelException(this.DataContext.GetType());

            viewModel.EditSelectedDepartment();
        }
    }
}
