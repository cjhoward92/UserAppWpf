﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserAppWpf.Framework;
using UserAppUser;

namespace UserAppWpf.Maintenance
{
    /// <summary>
    /// Interaction logic for MaintainUserView.xaml
    /// </summary>
    public partial class MaintainUserView : Window
    {
        public MaintainUserView()
        {
            InitializeComponent();
            this.Loaded += MaintainUserView_Loaded;
        }

        private void MaintainUserView_Loaded(object sender, RoutedEventArgs e)
        {
            var viewModel = this.DataContext as MaintainUserViewModel;
            viewModel.LoadDepartments();
        }
    }
}
