﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;

using UserAppWpf.Framework;
using UserAppWpf.Components;
using UserAppDepartment;
using UserAppCore.Data;
using UserAppDepartment.Commands;

namespace UserAppWpf.Maintenance
{
    public class MaintainDepartmentViewModel : MaintenanceViewModelBase<Department>
    {
        private readonly DelegateCommand saveCommand;
        private readonly DelegateCommand cancelCommand;
        private readonly ICommandHandler<SaveDepartmentCommand> saveDepartmentCommandHandler;
        private readonly IValidationProvider validationProvider;

        public DelegateCommand SaveCommand
        {
            get { return this.saveCommand; }
        }
        public DelegateCommand CancelCommand
        {
            get { return this.cancelCommand; }
        }

        #region Department Properties
        public string DepartmentName
        {
            get { return this.Model.DepartmentName; }
            set
            {
                this.Model.DepartmentName = value;
                NotifyPropertyChanged(nameof(this.Model.DepartmentName));
            }
        }
        public string DepartmentPhone
        {
            get { return this.Model.DepartmentPhone; }
            set
            {
                this.Model.DepartmentPhone = value;
                NotifyPropertyChanged(nameof(this.Model.DepartmentPhone));
            }
        }
        public string DepartmentAddress1
        {
            get { return this.Model.DepartmentAddress1; }
            set
            {
                this.Model.DepartmentAddress1 = value;
                NotifyPropertyChanged(nameof(this.Model.DepartmentAddress1));
            }
        }
        public string DepartmentAddress2
        {
            get { return this.Model.DepartmentAddress2; }
            set
            {
                this.Model.DepartmentAddress2 = value;
                NotifyPropertyChanged(nameof(this.Model.DepartmentAddress2));
            }
        }
        public string DepartmentCity
        {
            get { return this.Model.DepartmentCity; }
            set
            {
                this.Model.DepartmentCity = value;
                NotifyPropertyChanged(nameof(this.Model.DepartmentCity));
            }
        }
        public string DepartmentState
        {
            get { return this.Model.DepartmentState; }
            set
            {
                this.Model.DepartmentState = value;
                NotifyPropertyChanged(nameof(this.Model.DepartmentState));
            }
        }
        public string DepartmentZip
        {
            get { return this.Model.DepartmentZip; }
            set
            {
                this.Model.DepartmentZip = value;
                NotifyPropertyChanged(nameof(this.Model.DepartmentZip));
            }
        }
        #endregion

        public MaintainDepartmentViewModel(ICommandHandler<SaveDepartmentCommand> saveDepartmentCommandHandler,
            IValidationProvider validationProvider)
            : base()
        {
            this.saveCommand = new DelegateCommand(this.ExecuteSave, this.CanExecuteSave);
            this.cancelCommand = new DelegateCommand(this.ExecuteCancel, this.CanExecuteCancel);

            this.saveDepartmentCommandHandler = saveDepartmentCommandHandler;
            this.validationProvider = validationProvider;
        }

        private void Close(Window window)
        {
            window.Close();
        }
        private void Save()
        {
            try
            {
                this.TrySave();
            }
            catch (ValidationException ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }
        private void TrySave()
        {
            this.validationProvider.Validate(this.Model);

            SaveDepartmentCommand cmd = new SaveDepartmentCommand()
            {
                DepartmentToSave = this.Model
            };
            this.saveDepartmentCommandHandler.Handle(cmd);

            System.Windows.MessageBox.Show("Save successful");
        }

        #region Commands
        private bool CanExecuteSave(object parameter)
        {
            return true;
        }
        private void ExecuteSave(object parameter)
        {
            this.Save();
        }
        private bool CanExecuteCancel(object parameter)
        {
            return parameter is Window;
        }
        private void ExecuteCancel(object parameter)
        {
            this.Close(parameter as Window);
        }
        #endregion
    }
}
