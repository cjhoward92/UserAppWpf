﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UserAppWpf.Framework;
using UserAppUser;
using UserAppUser.Commands;
using UserAppWpf.Components;
using UserAppDepartment.Queries;
using UserAppDepartment;
using UserAppCore.Data;

namespace UserAppWpf.Maintenance
{
    public class MaintainUserViewModel : MaintenanceViewModelBase<User>
    {
        private readonly DelegateCommand saveCommand;
        private readonly DelegateCommand cancelCommand;
        private readonly IQueryHandler<GetAllDepartmentsQuery, IEnumerable<Department>> departmentQuery;
        private readonly ICommandHandler<SaveUserCommand> saveUserCommandHandler;
        private readonly IValidationProvider userValidationProvider;

        private List<Department> departments;

        public DelegateCommand SaveCommand
        {
            get { return this.saveCommand; }
        }
        public DelegateCommand CancelCommand
        {
            get { return this.cancelCommand; }
        }
        public List<Department> Departments
        {
            get { return this.departments; }
        }

        #region User 
        public string UserName
        {
            get { return this.Model.UserName; }
            set
            {
                this.Model.UserName = value;
                NotifyPropertyChanged(nameof(this.Model.UserName));
            }
        }
        public string Name
        {
            get { return this.Model.Name; }
            set
            {
                this.Model.Name = value;
                NotifyPropertyChanged(nameof(this.Model.Name));
            }
        }
        public string Email
        {
            get { return this.Model.Email; }
            set
            {
                this.Model.Email = value;
                NotifyPropertyChanged(nameof(this.Model.Email));
            }
        }
        public string Phone
        {
            get { return this.Model.Phone; }
            set
            {
                this.Model.Phone = value;
                NotifyPropertyChanged(nameof(this.Model.Phone));
            }
        }
        public bool Active
        {
            get { return this.Model.Active; }
            set
            {
                this.Model.Active = value;
                NotifyPropertyChanged(nameof(this.Model.Active));
            }
        }
        public int DepartmentId
        {
            get { return this.Model.DepartmentId; }
            set
            {
                this.Model.DepartmentId = value;
                NotifyPropertyChanged(nameof(this.Model.DepartmentId));
            }
        }
        #endregion

        public MaintainUserViewModel(IQueryHandler<GetAllDepartmentsQuery, IEnumerable<Department>> departmentQuery,
            ICommandHandler<SaveUserCommand> saveUserCommandHandler,
            IValidationProvider userValidationProvider)
            : base()
        {
            this.saveCommand = new DelegateCommand(this.ExecuteSave, this.CanExecuteSave);
            this.cancelCommand = new DelegateCommand(this.ExecuteCancel, this.CanExecuteCancel);

            this.departmentQuery = departmentQuery;
            this.saveUserCommandHandler = saveUserCommandHandler;
            this.userValidationProvider = userValidationProvider;
        }

        public void LoadDepartments()
        {
            this.departments = new List<Department>(this.departmentQuery.Handle(new GetAllDepartmentsQuery()));
            NotifyPropertyChanged(nameof(Departments));
        }
        private void Close(Window window)
        {
            window.Close();
        }
        private void Save()
        {
            try
            {
                this.TrySave();
            }
            catch (ValidationException ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }
        private void TrySave()
        {
            this.userValidationProvider.Validate(this.Model);

            var saveUserCommand = new SaveUserCommand()
            {
                UserToSave = this.Model
            };
            this.saveUserCommandHandler.Handle(saveUserCommand);

            System.Windows.MessageBox.Show("Save successful");
        }

        #region Commands
        private bool CanExecuteSave(object parameter)
        {
            return true;
        }
        private void ExecuteSave(object parameter)
        {
            this.Save();
        }
        private bool CanExecuteCancel(object parameter)
        {
            return parameter is Window;
        }
        private void ExecuteCancel(object parameter)
        {
            this.Close(parameter as Window);
        }
        #endregion
    }
}
