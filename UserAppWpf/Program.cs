﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Configuration;

using SimpleInjector;

using UserAppCore.Data;
using UserAppCore.Decorators;
using UserAppCore.Logging;
using UserAppCore;
using UserAppDepartment;
using UserAppUser.Queries;
using UserAppUser;
using UserAppWpf.Framework;
using UserAppWpf.Maintenance;
using UserAppWpf.Components;

using System.Diagnostics;

namespace UserAppWpf
{
    public class Program
    {
        private static Dictionary<string, Assembly> assemblies;

        [STAThread]
        static void Main()
        {
            var container = Bootstrap();
            Run(container);
        }

        public static Container Bootstrap()
        {
            LoadAssemblies();

            var container = new Container();

            RegisterGlobalConfig(container);
            RegisterSimple(container);
            RegisterOpenGenerics(container);
            RegisterFactories(container);
            RegisterSingletons(container);
            RegisterCollections(container);

            //container.Verify();

            return container;
        }

        #region Container Registration
        private static void RegisterGlobalConfig(Container container)
        {
            container.Register(
                () => new DatabaseConfigurationOptions() { ConnectionString = ConfigurationManager.ConnectionStrings["default"]?.ConnectionString ?? "x" });
        }
        private static void RegisterSimple(Container container)
        {
            container.Register<IDatabase, SqlDatabase>();
            container.Register<IQueryProcessor, QueryProcessor>();
            container.Register<ILogger, TextLogger>();
        }
        private static void RegisterOpenGenerics(Container container)
        {
            //Open Generics
            container.Register(typeof(IRepository<>), assemblies.Values);
            container.Register(typeof(IQuery<>), assemblies.Values);
            container.Register(typeof(IQueryHandler<,>), assemblies.Values);
            container.Register(typeof(ICommandHandler<>), assemblies.Values);
            container.Register(typeof(Validator<>), assemblies.Values);
            container.RegisterConditional(typeof(Validator<>), typeof(DefaultValidator<>), c => !c.Handled);
            container.Register(typeof(IMaintenanceViewModel<>), new[] { assemblies["this"] });
        }
        private static void RegisterFactories(Container container)
        {
            //ValidationProvider instantiation
            Func<Type, IValidator> validatorFactory = (type) =>
            {
                var validator = typeof(Validator<>).MakeGenericType(type);
                return (IValidator)container.GetInstance(validator);
            };
            container.Register<IValidationProvider>(() => new ValidationProvider(validatorFactory), Lifestyle.Transient);
        }
        private static void RegisterSingletons(Container container)
        {
            container.Register<IViewContainer, ViewContainer>(Lifestyle.Singleton);
            container.Register<IViewModelNavigator, ViewModelNavigator>(Lifestyle.Singleton);
            container.Register<IEditProcessor, EditProcessor>(Lifestyle.Singleton);
        }
        private static void RegisterCollections(Container container)
        {
            //Get all IViewModel types not in the Maintenance namespace
            var types = container.GetTypesToRegister(typeof(IViewModel),
                new[] { assemblies["this"] },
                new TypesToRegisterOptions() { IncludeGenericTypeDefinitions = false })
                .Where(t => !t.Namespace.Contains("Maintenance"));
            container.RegisterCollection<IViewModel>(types);

            //Get all of the windows
            var windowTypes = container.GetTypesToRegister(typeof(System.Windows.Window), new[] { assemblies["this"] });
            container.RegisterCollection(typeof(System.Windows.Window), windowTypes);

            container.RegisterCollection(typeof(ICommand), assemblies.Values);

        }
        #endregion

        private static void Run(Container container)
        {
            var window = new MainView();
            var vm = container.GetInstance<MainViewModel>();
            window.DataContext = vm;

            App app = new App();
            app.ShutdownMode = System.Windows.ShutdownMode.OnLastWindowClose;
            app.Run(window);
        }

        private static void LoadAssemblies()
        {
            Assembly thisAssmbly = Assembly.GetAssembly(typeof(Program));
            Assembly coreAssmbly = Assembly.GetAssembly(typeof(IDatabase));
            Assembly deptAssmbly = Assembly.GetAssembly(typeof(Department));
            Assembly userAssmbly = Assembly.GetAssembly(typeof(User));

            assemblies = new Dictionary<string, Assembly>();
            assemblies.Add("this", thisAssmbly);
            assemblies.Add("core", coreAssmbly);
            assemblies.Add("dept", deptAssmbly);
            assemblies.Add("user", userAssmbly);
        }
    }
}
