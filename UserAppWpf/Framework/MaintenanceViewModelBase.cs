﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAppWpf.Framework
{
    public abstract class MaintenanceViewModelBase<T> : IMaintenanceViewModel<T> where T : class
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string name) => this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        #endregion

        private Type viewModelType;

        protected MaintenanceViewModelBase()
        {
            this.model = null;
            this.viewModelType = this.GetType();
        }

        #region IViewModel
        private T model;
        public T Model
        {
            get
            {
                return this.model;
            }
        }

        public virtual void EditItem(T item)
        {
            this.model = item;
            this.NotifyPropertyChanged(nameof(model));
        }
        #endregion

        #region ICustomTypeDescriptor
        public virtual AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this.viewModelType);
        }
        public virtual String GetClassName()
        {
            return TypeDescriptor.GetClassName(this.viewModelType);
        }
        public virtual String GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this.viewModelType);
        }
        public virtual TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this.viewModelType);
        }
        public virtual EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this.viewModelType);
        }
        public virtual PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this.viewModelType);
        }
        public virtual Object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this.viewModelType, editorBaseType);
        }
        public virtual EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this.viewModelType);
        }
        public virtual EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this.viewModelType, attributes);
        }
        public virtual PropertyDescriptorCollection GetProperties()
        {
            return TypeDescriptor.GetProperties(this.viewModelType);
        }
        public virtual PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return TypeDescriptor.GetProperties(this.viewModelType, attributes);
        }
        public virtual Object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }
        #endregion
    }
}
