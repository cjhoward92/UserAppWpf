﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAppWpf.Framework.Exceptions
{
    public class InvalidViewModelException : Exception
    {
        public Type Type { get; private set; }

        public InvalidViewModelException(Type type)
            :base($"Type {type.FullName} is not a valid type")
        {
            this.Type = type;
        }
    }
}
