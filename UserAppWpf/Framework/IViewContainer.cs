﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace UserAppWpf.Framework
{
    public interface IViewContainer
    {
        void CreateInstanceFromViewModel(IViewModel viewModel);
    }
}
