﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleInjector;

namespace UserAppWpf.Framework
{
    public class EditProcessor : IEditProcessor
    {
        private readonly Container container;
        private readonly IViewContainer viewContainer;

        public EditProcessor(Container container, IViewContainer viewContainer)
        {
            this.container = container;
            this.viewContainer = viewContainer;
        }

        /// <summary>
        /// Builds the instance of the ViewModel and injects the model dynamically
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        public void EditItem<T>(T item)
        {
            var viewModel = typeof(IMaintenanceViewModel<>).MakeGenericType(typeof(T));
            dynamic instance = container.GetInstance(viewModel);

            instance.EditItem((dynamic)item);
            this.viewContainer.CreateInstanceFromViewModel(instance);
        }
    }
}
