﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAppWpf.Framework
{
    public interface IViewModelNavigator
    {
        void Navigate<T>() where T : IViewModel;
    }
}
