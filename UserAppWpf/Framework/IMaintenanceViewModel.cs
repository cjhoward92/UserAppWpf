﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace UserAppWpf.Framework
{
    public interface IMaintenanceViewModel<T> : IViewModel, INotifyPropertyChanged, ICustomTypeDescriptor where T : class
    {
        void EditItem(T item);
    }
}
