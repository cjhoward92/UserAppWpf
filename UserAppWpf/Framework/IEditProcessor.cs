﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleInjector;

namespace UserAppWpf.Framework
{
    public interface IEditProcessor
    {
        void EditItem<T>(T item);
    }
}
