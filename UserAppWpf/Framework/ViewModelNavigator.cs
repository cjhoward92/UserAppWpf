﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using SimpleInjector;

namespace UserAppWpf.Framework
{
    public class ViewModelNavigator : IViewModelNavigator
    {
        private readonly Container container;
        private readonly IViewContainer viewContainer;

        public ViewModelNavigator(Container container, IViewContainer viewContainer)
        {
            this.container = container;
            this.viewContainer = viewContainer;
        }

        public void Navigate<T>() where T : IViewModel
        {
            var type = typeof(T);
            var instance = container.GetInstance(type) as IViewModel;
            this.viewContainer.CreateInstanceFromViewModel(instance);
        }
    }
}
