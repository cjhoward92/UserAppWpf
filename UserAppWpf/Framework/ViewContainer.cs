﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace UserAppWpf.Framework
{
    public class ViewContainer : IViewContainer
    {
        private Dictionary<Type, Type> cache;

        public ViewContainer()
        {
            this.cache = new Dictionary<Type, Type>();
        }

        public void CreateInstanceFromViewModel(IViewModel viewModel)
        {
            if (viewModel == null)
                throw new ArgumentNullException();

            var windowType = this.GetWindowType(viewModel.GetType());
            this.CreateInstance(windowType, viewModel);
        }

        private Type GetWindowType(Type viewModelType)
        {
            if (this.cache.ContainsKey(viewModelType))
                return this.cache[viewModelType];

            var assmbly = viewModelType.Assembly;
            string typeName = viewModelType.FullName;

            if (!typeName.EndsWith("Model"))
                throw new Exceptions.InvalidViewModelException(viewModelType);

            typeName = typeName.Substring(0, typeName.Length - 5);
            Type windowType = assmbly.GetType(typeName);

            this.cache.Add(viewModelType, windowType);
            return windowType;
        }

        private void CreateInstance(Type windowType, Object viewModel)
        {
            Window window = Activator.CreateInstance(windowType) as Window;
            window.DataContext = viewModel;
            window.Show();
            window.Activate();
        }
    }
}
