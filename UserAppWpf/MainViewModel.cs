﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppWpf.Framework;
using UserAppWpf.Components;
using UserAppWpf.Management;

namespace UserAppWpf
{
    public class MainViewModel : ViewModelBase
    {
        private readonly DelegateCommand usersCommand;
        private readonly DelegateCommand departmentsCommand;
        private readonly IViewModelNavigator navigator;

        public DelegateCommand UsersCommand
        {
            get { return this.usersCommand; }
        }
        public DelegateCommand DepartmentsCommand
        {
            get { return this.departmentsCommand; }
        }

        public MainViewModel(IViewModelNavigator navigator)
        {
            this.usersCommand = new DelegateCommand(this.ExecuteUsers, (p) => true);
            this.departmentsCommand = new DelegateCommand(this.ExecuteDepartments, (p) => true);

            this.navigator = navigator;
        }

        #region Commands
        private void ExecuteUsers(object parameter)
        {
            this.navigator.Navigate<UserManagementViewModel>();
        }
        private void ExecuteDepartments(object parameter)
        {
            this.navigator.Navigate<DepartmentManagementViewModel>();
        }
        #endregion
    }
}
