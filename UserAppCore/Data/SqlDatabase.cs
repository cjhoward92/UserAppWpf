﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace UserAppCore.Data
{
    public class SqlDatabase : IDatabase
    {
        private readonly DatabaseConfigurationOptions options;

        public DatabaseConfigurationOptions Options
        {
            get
            {
                return this.Options;
            }
        }

        public SqlDatabase(DatabaseConfigurationOptions options)
        {
            this.options = options;
        }

        public IDbConnection CreateConnection()
        {
            if (this.options == null || String.IsNullOrWhiteSpace(this.options.ConnectionString))
                throw new InvalidOperationException($"A connection string is required");

            return new SqlConnection(this.options.ConnectionString);
        }
    }
}
