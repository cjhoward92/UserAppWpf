﻿using System;
using System.Collections.Generic;

namespace UserAppCore.Data
{
    public interface IValidator
    {
        IEnumerable<ValidationResult> Validate(object entity);
    }
}
