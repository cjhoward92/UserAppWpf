﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace UserAppCore.Data
{
    public class ValidationException : Exception
    {
        public ReadOnlyCollection<ValidationResult> Errors { get; private set; }

        public ValidationException(IEnumerable<ValidationResult> results)
            :base(GetFirstErrorMessage(results))
        {
            this.Errors = results.ToList().AsReadOnly();
        }

        private static string GetFirstErrorMessage(
        IEnumerable<ValidationResult> errors)
        {
            return errors.First().Message;
        }
    }
}
