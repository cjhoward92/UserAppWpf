﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAppCore.Data
{
    public class DatabaseConfigurationOptions
    {
        public virtual string ConnectionString { get; set; }
        public virtual bool LoadChildDataImmediately { get; set; }

        public DatabaseConfigurationOptions()
        {
            this.ConnectionString = null;
            this.LoadChildDataImmediately = false;
        }
    }
}
