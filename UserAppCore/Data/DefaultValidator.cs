﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UserAppCore.Data
{
    public sealed class DefaultValidator<T> : Validator<T>
    {
        protected override IEnumerable<ValidationResult> Validate(T entity)
        {
            return Enumerable.Empty<ValidationResult>();
        }
    }
}
