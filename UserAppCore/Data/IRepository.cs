﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace UserAppCore.Data
{
    public interface IRepository<T> where T : class
    {
        T GetById(IDbConnection cn, int id);
        IEnumerable<T> GetAll(IDbConnection cn);

        void Insert(IDbConnection cn, T entity);
        void Update(IDbConnection cn, T entity);
        void Delete(IDbConnection cn, T entity);
    }
}
