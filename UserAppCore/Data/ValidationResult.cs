﻿using System;

namespace UserAppCore.Data
{
    public class ValidationResult
    {
        public string Key { get; private set; }
        public string Message { get; private set; }

        public ValidationResult(string key, string message)
        {
            this.Key = key;
            this.Message = message;
        }
    }
}
