﻿using System;

namespace UserAppCore.Data
{
    public interface IQueryProcessor
    {
        /// <summary>
        /// Dynamically handles a query
        /// </summary>
        /// <typeparam name="TResult">The type of result from the specified query</typeparam>
        /// <param name="query">The query to handle</param>
        /// <returns>The result of the handled query</returns>
        TResult Process<TResult>(IQuery<TResult> query);
    }
}
