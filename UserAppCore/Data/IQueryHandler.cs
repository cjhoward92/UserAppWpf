﻿using System;

namespace UserAppCore.Data
{
    public interface IQueryHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    {
        /// <summary>
        /// Handles a query and returns a result
        /// </summary>
        /// <param name="query">The query to handle</param>
        /// <returns>The result of the handled query</returns>
        TResult Handle(TQuery query);
    }
}
