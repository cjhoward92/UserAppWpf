﻿using System;
using System.Data;

namespace UserAppCore.Data
{
    public interface IDatabase
    {
        DatabaseConfigurationOptions Options { get; }
        IDbConnection CreateConnection();
    }
}
