﻿using System;

namespace UserAppCore.Data
{
    /// <summary>
    /// This object acts as a filter group for queries
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public interface IQuery<TResult>
    {
    }
}
