﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserAppCore.Data
{
    public abstract class Validator<T> : IValidator
    {
        public IEnumerable<ValidationResult> Validate(Object entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            return this.Validate((T)entity);
        }

        protected abstract IEnumerable<ValidationResult> Validate(T entity);
    }
}
