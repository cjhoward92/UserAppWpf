﻿using System;
using System.Collections;

namespace UserAppCore.Data
{
    public interface IValidationProvider
    {
        void Validate(object entity);
        void ValidateAll(IEnumerable entities);
    }
}
