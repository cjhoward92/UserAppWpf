﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UserAppCore.Logging
{
    public class TextLogger : ILogger
    {
        private readonly TextLogContext context;

        public TextLogger(TextLogContext context)
        {
            this.context = context;
        }

        public void Log(String message)
        {
            if (String.IsNullOrWhiteSpace(message))
                throw new ArgumentNullException(nameof(message));

            File.WriteAllText(this.context.FilePath, message);
        }
    }
}
