﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace UserAppCore.Logging
{
    public class TextLogContext
    {
        public virtual string FilePath
        {
            get
            {
                return ConfigurationManager.AppSettings["TextLogFilePath"];
            }
        }
    }
}
