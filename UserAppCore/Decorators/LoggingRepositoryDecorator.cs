﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using UserAppCore.Data;
using UserAppCore.Logging;

namespace UserAppCore.Decorators
{
    public class LoggingRepositoryDecorator<T> : IRepository<T> where T : class
    {
        private readonly IRepository<T> repository;
        private readonly ILogger logger;

        public LoggingRepositoryDecorator(IRepository<T> repository, ILogger logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        public void Delete(IDbConnection cn, T entity)
        {
            this.repository.Delete(cn, entity);
            this.logger.Log($"{nameof(this.Delete)} success");
        }

        public IEnumerable<T> GetAll(IDbConnection cn)
        {
            var xCol = this.repository.GetAll(cn);
            this.logger.Log($"{nameof(this.GetAll)} success");
            return xCol;
        }

        public T GetById(IDbConnection cn, Int32 id)
        {
            var x = this.repository.GetById(cn, id);
            this.logger.Log($"{nameof(this.GetById)} success");
            return x;
        }

        public void Insert(IDbConnection cn, T entity)
        {
            this.repository.Insert(cn, entity);
            this.logger.Log($"{nameof(this.Insert)} success");
        }

        public void Update(IDbConnection cn, T entity)
        {
            this.repository.Update(cn, entity);
            this.logger.Log($"{nameof(this.Update)} success");
        }
    }
}
