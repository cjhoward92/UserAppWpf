﻿using System;
using UserAppCore.Logging;

namespace UserAppCore.Decorators
{
    public class DateTimeLoggerDecorator : ILogger
    {
        private readonly ILogger logger;

        public DateTimeLoggerDecorator(ILogger logger)
        {
            this.logger = logger;
        }

        public void Log(String message)
        {
            if (String.IsNullOrWhiteSpace(message))
                throw new ArgumentNullException(nameof(message));

            message += $"\r\nTime: {DateTime.UtcNow}";
            this.logger.Log(message);
        }
    }
}
