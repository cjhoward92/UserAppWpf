﻿using System;
using UserAppCore.Data;

namespace UserAppCore.Decorators
{
    public class ValidationQueryHandlerDecorator<TQuery, TResult> : IQueryHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    {
        private readonly IQueryHandler<TQuery, TResult> queryHandler;

        public ValidationQueryHandlerDecorator(IQueryHandler<TQuery, TResult> queryHandler)
        {
            this.queryHandler = queryHandler;
        }

        public TResult Handle(TQuery query)
        {
            var validationContext = new System.ComponentModel.DataAnnotations.ValidationContext(query, null, null);
            System.ComponentModel.DataAnnotations.Validator.ValidateObject(query, validationContext, true);
            return this.queryHandler.Handle(query);
        }
    }
}
