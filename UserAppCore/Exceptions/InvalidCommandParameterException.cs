﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAppCore.Data;

namespace UserAppCore.Exceptions
{
    public class InvalidCommandParameterException : Exception
    {
        public string ParameterName { get; private set; }
        public object ParameterValue { get; private set; }
        public ICommand Command { get; private set; }

        public InvalidCommandParameterException(ICommand command, string parameterName, object parameterValue)
            :base($"Command {command.GetType().Name} contains the invalid parameter {parameterName}")
        {
            this.Command = command;
            this.ParameterName = parameterName;
            this.ParameterValue = parameterValue;
        }
    }
}
