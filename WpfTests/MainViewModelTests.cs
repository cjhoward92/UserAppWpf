﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using UserAppWpf;
using UserAppWpf.Framework;
using UserAppWpf.Maintenance;
using UserAppWpf.Management;

namespace WpfTests
{
    [TestClass]
    public class MainViewModelTests
    {
        [TestMethod]
        public void Test_MainviewModel_UserCommand()
        {
            bool result = false;
            var subNav = Substitute.For<IViewModelNavigator>();
            subNav.When(n => n.Navigate<UserManagementViewModel>()).Do((c) => { result = true; });

            MainViewModel model = new MainViewModel(subNav);
            model.UsersCommand.Execute(null);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Test_MainviewModel_DepartmentCommand()
        {
            bool result = false;
            var subNav = Substitute.For<IViewModelNavigator>();
            subNav.When(n => n.Navigate<DepartmentManagementViewModel>()).Do((c) => { result = true; });

            MainViewModel model = new MainViewModel(subNav);
            model.DepartmentsCommand.Execute(null);
            Assert.IsTrue(result);
        }
    }
}
