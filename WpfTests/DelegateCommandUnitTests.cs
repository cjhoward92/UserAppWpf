﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppWpf.Components;
using UserAppWpf;
using NSubstitute;

namespace WpfTests
{
    [TestClass]
    public class DelegateCommandUnitTests
    {
        private Func<object, bool> MockCanExecute()
        {
            return (obj) => true;
        }
        private Action<object> MockExecute()
        {
            return (o) => o = true;
        }

        [TestMethod]
        public void Test_DelegateCommand_CanExecute()
        {
            bool result = false;
            Func<object, bool> canExecute = (obj) =>
            {
                return true;
            };

            var cmd = new DelegateCommand(MockExecute(), canExecute);
            result = cmd.CanExecute(null);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Test_DelegateCommand_Execute()
        {
            bool result = false;
            Action<object> execute = (obj) =>
            {
                result = true;
            };

            var cmd = new DelegateCommand(execute, MockCanExecute());
            cmd.Execute(null);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Test_DelegateCommand_CanExecuteChanged()
        {
            bool result = false;
            
            var cmd = new DelegateCommand(MockExecute(), MockCanExecute());
            cmd.CanExecuteChanged += (o, s) =>
            {
                result = true;
            };

            cmd.RaiseCanExecuteChanged();

            Assert.IsTrue(result);
        }
    }
}
