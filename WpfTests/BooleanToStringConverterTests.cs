﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppWpf.Converters;

namespace WpfTests
{
    [TestClass]
    public class BooleanToStringConverterTests
    {
        [TestMethod]
        public void Test_BooleantoStringConverter_Convert()
        {
            BooleanToStringConverter converter = new BooleanToStringConverter();

            bool b = true;
            string s;
            s = converter.Convert(b, typeof(string), null, null) as string;
            Assert.AreEqual(b.ToString(), s);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_BooleantoStringConverter_ConvertNull()
        {
            BooleanToStringConverter converter = new BooleanToStringConverter();

            string s;
            s = converter.Convert(null, typeof(string), null, null) as string;
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        public void Test_BooleantoStringConverter_ConvertNonBool()
        {
            BooleanToStringConverter converter = new BooleanToStringConverter();

            string s;
            s = converter.Convert(1000, typeof(string), null, null) as string;
        }

        [TestMethod]
        public void Test_BooleantoStringConverter_ConvertBack()
        {
            BooleanToStringConverter converter = new BooleanToStringConverter();

            bool b;
            string s = "true";
            b = (bool)converter.ConvertBack(s, typeof(bool), null, null);
            Assert.AreEqual(true, b);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        public void Test_BooleantoStringConverter_ConvertBackNonString()
        {
            BooleanToStringConverter converter = new BooleanToStringConverter();

            bool b;
            b = (bool)converter.ConvertBack(10000, typeof(bool), null, null);
        }
    }
}
