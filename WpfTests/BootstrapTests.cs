﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserAppWpf;
using SimpleInjector;
using NSubstitute;

namespace WpfTests
{
    [TestClass]
    public class BootstrapTests
    {
        [TestMethod]
        public void Test_BootStrap()
        {
            var container = Program.Bootstrap();
            container.Verify(VerificationOption.VerifyAndDiagnose);
            Assert.IsTrue(true);
        }
    }
}
